package com.buttercat.algorythmhub.view;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.buttercat.algorythmhub.R;
import com.buttercat.algorythmhub.databinding.AddNodeDialogBinding;
import com.buttercat.algorythmhub.databinding.NodeListFragmentBinding;
import com.buttercat.algorythmhub.viewmodel.NodeListViewModel;

/**
 * A custom {@link Fragment} which shows the
 * {@link com.buttercat.algorythmhub.view.utils.NodeListViewAdapter}
 */
public class NodeListFragment extends Fragment {
    /**
     * The databinding class which takes care of inflating the fragment
     */
    private NodeListFragmentBinding mBinding;

    /**
     * Method returning a new instance of this class
     *
     * @return a new {@link NodeListFragment} instance
     */
    /*package*/ static NodeListFragment newInstance() {
        return new NodeListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBinding = NodeListFragmentBinding
                .inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        NodeListViewModel mViewModel = new ViewModelProvider(this).get(NodeListViewModel.class);
        mBinding.nodeList.setHasFixedSize(true);
        mBinding.nodeList.setLayoutManager(new LinearLayoutManager(mBinding.nodeList.getContext()));
        mBinding.nodeList.setAdapter(mViewModel.getAdapter());
        mBinding.setLifecycleOwner(this);

        mBinding.addNode.setOnClickListener((v) -> {
//
            AddNodeDialogBinding binding = DataBindingUtil
                    .inflate(LayoutInflater.from(getContext()), R.layout.add_node_dialog, null, false);

            Dialog dialog = new Dialog(getContext());
            dialog.setContentView(binding.getRoot());
            dialog.show();
            binding.updateButton.setOnClickListener((bv) -> {
                mViewModel.onAddClicked(binding.editIp.getText().toString());
                dialog.dismiss();
            });
        });


        mViewModel.updateNodeList();
        mBinding.pullToRefresh.setOnRefreshListener(() -> {
            mViewModel.updateNodeList();
            mBinding.pullToRefresh.setRefreshing(false);
        });
    }
}

