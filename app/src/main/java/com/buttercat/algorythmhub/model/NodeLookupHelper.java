package com.buttercat.algorythmhub.model;

import android.content.SharedPreferences;
import android.net.DhcpInfo;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.lifecycle.*;
import com.buttercat.algorythmhub.model.definitions.Color;
import com.buttercat.algorythmhub.model.definitions.ESP32Node;
import com.buttercat.algorythmhub.model.definitions.Mode;
import com.buttercat.algorythmhub.model.definitions.Prefs;
import org.jetbrains.annotations.NotNull;
import timber.log.Timber;
import org.apache.commons.net.util.*;

import java.io.IOException;

import java.net.*;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class NodeLookupHelper implements LifecycleOwner {

    /**
     * Singleton instance of this class
     */
    private static NodeLookupHelper sInstance;
    private final SharedPreferences mPrefs;
    private LifecycleRegistry mLifecycleRegistry = new LifecycleRegistry(this);
    private Context mContext;
    MutableLiveData<ESP32Node> mNodesLiveData;
    private final Executor mExecutor;
    private final Handler mMainThread = new Handler(Looper.getMainLooper());

    /**
     * Obtain a singleton {@link NodeLookupHelper}
     *
     * @param appContext a {@link Context} used to obtain a {@link NodeLookupHelper} instance
     * @param executors {@link AppExecutors} used to obtain the executor handling operations
     *
     * @return a singleton instance of the {@link NodeLookupHelper}
     */
    public static NodeLookupHelper getInstance(final Context appContext, final AppExecutors executors, final SharedPreferences sharedPreferences) {
        synchronized (NodeLookupHelper.class) {
            if (sInstance == null) {
                sInstance = new NodeLookupHelper(appContext, executors, sharedPreferences);
            }
        }
        return sInstance;
    }

    /**
     *
     *
     * @param appContext a {@link Context} used to obtain a {@link NodeLookupHelper} instance
     * @param executors the {@link java.util.concurrent.Executors} to use to run operations within {@link NodeLookupHelper}
     */
    private NodeLookupHelper(final Context appContext,
                             final AppExecutors executors, final SharedPreferences sharedPreferences) {
        mNodesLiveData = new MutableLiveData<>();
        mExecutor = executors.getNetExecutorService();
        mLifecycleRegistry.setCurrentState(Lifecycle.State.STARTED);
        mContext = appContext;
        mPrefs = sharedPreferences;
    }

    public static ESP32Node createNodeFromHost(String ip, String hostname) {
        String room = "Undefined";
        Mode mode = Mode.OFF;

        return new ESP32Node(hostname, ip, room, mode, new Prefs(), new Color(android.graphics.Color.TRANSPARENT));
    }

    public void discoverNetwork() {
        mNodesLiveData.setValue(null);

        for (Map.Entry<String, ?> entry: mPrefs.getAll().entrySet()) {
            String ip = entry.getKey();
            addNodeFromIp(ip);
            Timber.d("Shared Preferences entry: %s",entry.getKey());
        }
        DhcpInfo dhcpInfo;
        WifiManager wifiMgr;

        wifiMgr= (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (!wifiMgr.isWifiEnabled()) return;
        dhcpInfo=wifiMgr.getDhcpInfo();

        InetAddress address;
        int netmaskBits = 0;

        NetworkInterface networkInterface = null;
        try {
            address  = InetAddress.getByName(intToIp(dhcpInfo.ipAddress));
            networkInterface = NetworkInterface.getByInetAddress(address );
            for (InterfaceAddress addr : networkInterface.getInterfaceAddresses()) {

                short netPrefix = addr.getNetworkPrefixLength();
                // limited to IPv4
                if (netPrefix < 32) {
                    netmaskBits = netPrefix;
                }

            }
        } catch (UnknownHostException | SocketException e) {

            Timber.e(e);
        }

        int mask = ~(0xffffffff << (netmaskBits));
        Timber.d(intToIp(dhcpInfo.ipAddress & mask));
        Timber.d("net prefix: %s", netmaskBits);

        SubnetUtils utils = new SubnetUtils(intToIp(dhcpInfo.ipAddress)+"/"+netmaskBits);
        String[] allIps = utils.getInfo().getAllAddresses();

        ExecutorService discoveryService = Executors.newFixedThreadPool(allIps.length);


        for (String ip : allIps) {
            discoveryService.execute(() -> {
                boolean reachable = isAddrReachable(ip);
                if (reachable) {
                    InetAddress reachedIp = null;
                    try {
                        reachedIp = InetAddress.getByName(ip);
                        Timber.d("Reachable IP %s has hostname: %s",ip, reachedIp.getHostName());
                        ESP32Node esp32Node = createNodeFromHost(ip, reachedIp.getHostName());
                        mMainThread.postDelayed(() -> {
                            mNodesLiveData.setValue(esp32Node);
                        }, 500);

                    } catch (UnknownHostException e) {
                        Timber.e(e);
                    }

                }
            });
        }
    }

    public String intToIp(int addr) {
        return  ((addr & 0xFF) + "." +
                ((addr >>>= 8) & 0xFF) + "." +
                ((addr >>>= 8) & 0xFF) + "." +
                ((addr >>>= 8) & 0xFF));
    }

    public boolean addNodeFromIp(String ip) {
        AtomicBoolean result = new AtomicBoolean(false);
        mExecutor.execute(() -> {
            if (!isAddrReachable(ip)) return;
            String reachedIpHostname = null;
            try {
                reachedIpHostname = InetAddress.getByName(ip).getHostName();
                Timber.d("Reachable IP %s has hostname: %s",ip, reachedIpHostname);
                ESP32Node esp32Node = createNodeFromHost(ip, reachedIpHostname);

                String finalReachedIpHostname = reachedIpHostname;

                mMainThread.postDelayed(() -> {
                    mNodesLiveData.setValue(esp32Node);
                }, 100);
                result.set(true);
            } catch (UnknownHostException e) {
                Timber.e(e);
            }
        });
        return result.get();
    }

    private boolean isAddrReachable(String ip) {
        Runtime runtime = Runtime.getRuntime();
        boolean reachable = false;
        try {
            Process  mIpAddrProcess = runtime.exec("/system/bin/ping -c 1 "+ip);
            int exitValue = mIpAddrProcess.waitFor();
            if (exitValue == 0) {
                reachable = true;
            }
        }  catch (InterruptedException | IOException error) {
            Timber.e(error);
        } finally {
            runtime.freeMemory();
        }
        return reachable;
    }

    public LiveData<ESP32Node> getNodesLiveData() {
        return mNodesLiveData;
    }

    @NonNull
    @NotNull
    @Override
    public Lifecycle getLifecycle() {
        return mLifecycleRegistry;
    }
}